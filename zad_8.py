# -*- coding: utf-8 -*-
"""
Created on Thu Jan 07 13:45:02 2016

@author: Filip
"""

import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from sklearn.metrics import confusion_matrix


x_train = np.array(200)
x_test = np.array(100)

def generate_data(n):
    
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data

np.random.seed(242)
x_train = generate_data(200)
np.random.seed(12)
x_test = generate_data(100)

#Skaliranje
x_train_scaled = preprocessing.scale(x_train[:,0:2])
x_test_scaled = preprocessing.scale(x_test[:,0:2])


#Izgradnja modela
KNN_model = KNeighborsClassifier(n_neighbors=3)
KNN_model.fit(x_train_scaled[:,0:2], x_train[:,2])

KNN_model_predict = KNN_model.predict(x_test_scaled[:,0:2])



def plot_KNN(KNN_model, X, y):
    
    x1_min, x1_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    x2_min, x2_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    
    xx, yy = np.meshgrid(np.arange(x1_min, x1_max, 0.01),
                         np.arange(x2_min, x2_max, 0.01))
                         
    Z1 = KNN_model.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z1.reshape(xx.shape)
    plt.figure()
    plt.pcolormesh(xx, yy, Z, cmap='PiYG', vmin = -2, vmax = 2)
    plt.scatter(X[:,0], X[:,1], c = y, s = 30, marker= 'o' , cmap='RdBu',
                edgecolor='white', label = 'train')
                
                
plot_KNN(KNN_model,x_train_scaled[:,0:2], x_train[:,2])


c_matrix_KNN = confusion_matrix(x_test[:,2], KNN_model_predict)


def plot_confusion_matrix_KNN(c_matrix_KNN):
    norm_conf = []
    for i in c_matrix_KNN:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    
    width = len(c_matrix_KNN)
    height = len(c_matrix_KNN[0])
    
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix_KNN[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
                        
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

plot_confusion_matrix_KNN(c_matrix_KNN)
