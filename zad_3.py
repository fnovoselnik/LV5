# -*- coding: utf-8 -*-
"""
Created on Fri Jan 08 15:24:29 2016

@author: Filip
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import PolynomialFeatures

x_train = np.array(200)
x_test = np.array(100)

def generate_data(n):
    
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data

np.random.seed(242)
x_train = generate_data(200)
np.random.seed(12)
x_test = generate_data(100)


#zad 3

#Izgradnja modela
linearModel = lm.LogisticRegression()
linearModel.fit(x_test[:,0:2], x_test[:,2])

#
theta0 =  linearModel.intercept_
theta1 = linearModel.coef_[0,0]
theta2 = linearModel.coef_[0,1]

x1 = np.linspace(-10,10,100)

x_test_pred =  linearModel.predict(x_test[:,0:2])


#Funkcija za crtanje linearne granice izmedu klasa
def granica(x1,theta0,theta1,theta2):
    x2 = (1/theta2)*(-theta0-(theta1*x1))
    return x2
    
x2 = granica(x1,theta0,theta1,theta2)

fig, ax = plt.subplots()
im = ax.scatter(x_test[:,0], x_test[:,1], c=x_test[:,2], s=100, cmap=plt.cm.jet)
fig.colorbar(im, ax=ax)
plt.plot(x1,x2)