# -*- coding: utf-8 -*-
"""
Created on Thu Jan 07 10:42:55 2016

@author: Filip
"""

import numpy as np
import matplotlib.pyplot as plt
import sklearn.linear_model as lm
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import PolynomialFeatures

x_train = np.array(200)
x_test = np.array(100)

def generate_data(n):
    
    #prva klasa
    n1 = n/2
    x1_1 = np.random.normal(0.0, 2, (n1,1));
    #x1_1 = .21*(6.*np.random.standard_normal((n1,1)));
    x2_1 = np.power(x1_1,2) + np.random.standard_normal((n1,1));
    y_1 = np.zeros([n1,1])
    temp1 = np.concatenate((x1_1,x2_1,y_1),axis = 1)
    #druga klasa
    n2 = n - n/2
    x_2 = np.random.multivariate_normal((0,10), [[0.8,0],[0,1.2]], n2);
    y_2 = np.ones([n2,1])
    temp2 = np.concatenate((x_2,y_2),axis = 1)
    data = np.concatenate((temp1,temp2),axis = 0)
    #permutiraj podatke
    indices = np.random.permutation(n)
    data = data[indices,:]
    return data

np.random.seed(242)
x_train = generate_data(200)
np.random.seed(12)
x_test = generate_data(100)


#zad 7

poly = PolynomialFeatures(degree=3, include_bias = False)
data_train_new = poly.fit_transform(x_train[:,0:2])
data_test_new = poly.fit_transform(x_test[:,0:2])

linearModel = lm.LogisticRegression()
linearModel.fit(data_train_new, x_train[:,2])

#theta0 =  linearModel.intercept_
#theta1 = linearModel.coef_[0,0]
#theta2 = linearModel.coef_[0,1]

x_test_pred =  linearModel.predict(data_test_new)

c_matrix = confusion_matrix(x_test[:,2], x_test_pred)

TP  = float(c_matrix[0][0])
FP  = float(c_matrix[0][1])
FN  = float(c_matrix[1][0])
TN  = float(c_matrix[1][1])

def confusion_matrix_validation(TP,FP,FN,TN):
    acc = (TP+TN) / (TP+TN+FP+FN)
    miss_rate = 1 - acc
    prec = TP / TP+FP
    recall = TP / TP+FN
    spec = TN / TN+FP
    print 'Accuracy: ', acc
    print 'Missclasification rate: ', miss_rate
    print 'Precision: ', prec
    print 'Recall/Sensitivity: ', recall
    print 'Specificity: ', spec
    

def plot_confusion_matrix(c_matrix):
    norm_conf = []
    for i in c_matrix:
        a = 0
        tmp_arr = []
        a = sum(i, 0)
        for j in i:
            tmp_arr.append(float(j)/float(a))
        norm_conf.append(tmp_arr)
    fig = plt.figure()
    ax = fig.add_subplot(111)
    res = ax.imshow(np.array(norm_conf), cmap=plt.cm.Greys, interpolation='nearest')
    
    width = len(c_matrix)
    height = len(c_matrix[0])
    
    for x in xrange(width):
        for y in xrange(height):
            ax.annotate(str(c_matrix[x][y]), xy=(y, x),
                        horizontalalignment='center',
                        verticalalignment='center', color = 'green', size = 20)
                        
    fig.colorbar(res)
    numbers = '0123456789'
    plt.xticks(range(width), numbers[:width])
    plt.yticks(range(height), numbers[:height])
    plt.ylabel('Stvarna klasa')
    plt.title('Predvideno modelom')
    plt.show()

plot_confusion_matrix(c_matrix)
confusion_matrix_validation(TP,FP,FN,TN)


#
